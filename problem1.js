const fs = require('fs'); 
const path = require('path'); 

function problem1(data={}, dirName="jsonDir", numOfFiles=1){
    const dirPath = path.join(__dirname, dirName);

    // creating directory
    fs.mkdir(dirPath, (dirError) => {
        if (dirError) {
            throw dirError;
        } else {
            console.log(`${dirName} Directory Created!`);

            // creating json files
            for (let index = 0; index < numOfFiles; index++) {
                fs.writeFile(
                    `${dirPath}/test${index + 1}.json`,
                    JSON.stringify(data),
                    (fileError) => {
                        if (fileError) {
                            throw fileError;
                        } else {
                            console.log(`test${index + 1}.json File Created!`);

                            // deleting json files
                            fs.unlink(
                                `${dirPath}/test${index + 1}.json`,
                                (rmError) => {
                                    if (rmError) {
                                        throw rmError;
                                    } else {
                                        console.log(`test${index + 1}.json File Deleted!`);
                                    }
                                }
                            );
                        }
                    }
                );
            }
        }
    }); 
}

module.exports = problem1; 