const fs = require("fs");

function problem2(file) {
    const upperCaseFileName = upperCaseContent(file);
    const lowerCaseSplitFileName = lowerCaseSplitContent(upperCaseFileName);
    
    // sorting data in upperCaseFile & lowerCaseSplitFile
    fs.readFile(upperCaseFileName, "utf-8", (error, upperCaseFileName) => {
        if (error) {
            throw error;
        } else {
            fs.readFile(lowerCaseSplitFileName, "utf-8", (error, lowerCaseSplitFileName) => {
                if (error) {
                    throw error;
                } else {
                    let sortFileData = (upperCaseFileName + lowerCaseSplitFileName)
                        .replace("\n", " ")
                        .split(" ")
                        .map((word) => word.trim())
                        .sort()
                        .join(" ");

                    writeFileContent("sortedFileContent.txt", sortFileData);
                    
                    fs.appendFile("fileNames.txt", 'sortedFileContent.txt\n', (error) => {
                        if (error) {
                            throw error;
                        } else {
                            console.log("sortedFileContent.txt File Added!");

                            // reading files in fileNames.txt
                            fs.readFile(
                                "fileNames.txt",
                                "utf-8",
                                (error, data) => {
                                    if (error) {
                                        throw error;
                                    } else {
                                        // deleting files present in fileNames.txt
                                        data.trim().split("\n").forEach(fileName => {
                                            fs.unlink(fileName, (error) => {
                                                if(error){
                                                    throw error; 
                                                }
                                                else {
                                                    console.log(`${fileName} Deleted!`); 
                                                }
                                            }); 
                                        });
                                    }
                                }
                            );
                        }
                    });
                }
            });
        }
    });
}

function upperCaseContent(file) {
    const upperCaseContent = file.toUpperCase();
    const fileName = "upperCaseContent.txt";

    writeFileContent(fileName, upperCaseContent);
    writeFileNames(fileName);

    return fileName;
}

function lowerCaseSplitContent(file) {
    const fileName = "lowerCaseSplitContent.txt";
    fs.readFile(file, "utf-8", (error, data) => {
        if (error) {
            throw error;
        } else {
            let lowerCaseSplitData = data
                .toLowerCase()
                .split(".")
                .map((line) => {
                    return line.trim(); 
                })
                .join("\n");

            writeFileContent(fileName, lowerCaseSplitData);
            writeFileNames(fileName);
        }
    });
    return fileName;
}

function writeFileContent(fileName, fileData) {
    fs.writeFile(fileName, fileData, (error) => {
        if (error) {
            throw error;
        } else {
            console.log(fileName + " File Created!");
        }
    });
}

function writeFileNames(fileName) {
    fs.appendFile("fileNames.txt", `${fileName}\n`, (error) => {
        if (error) {
            throw error;
        } else {
            console.log(fileName + " File Added!");
        }
    });
}

module.exports = problem2;
