const problem2 = require('../problem2'); 
const fs = require('fs'); 
const path = require('path'); 
const filePath = path.join(__dirname, '../lipsum.txt'); 

fs.readFile(filePath, 'utf-8', (error, data) => {
    if (error) {
        throw error; 
    }
    else {
        problem2(data);  
    }
})
